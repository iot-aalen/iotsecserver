package de.hsaalen.iot.database;

import de.hsaalen.iot.crypto.AsymmetricKeys;
import de.hsaalen.iot.crypto.SHA256;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import de.hsaalen.iot.util.Util;



import java.io.IOException;
import java.util.Arrays;

// Private und Public Key laden
public class Keys {
    public static final int  NUM_SERVER_ID_SMALL = 4;

    private  AsymmetricKeyParameter AKPublic;
    private  AsymmetricKeyParameter AKPrivate;
    private byte[] ServerIDsmall; //set in createServerIDsmall

    /**
     * Lädt die Keys für den Server vom Fileverzeichnis nach und erzeugt den Hashwert
     * @param publicKeyFileName   - Pfad inkl. Filename für PublicKey-File
     * @param privateKeyFileName  - Pfad inkl. Filename für PrivateKey-File
     * @return Keys
     */
    public static Keys load(String publicKeyFileName, String privateKeyFileName) {
        AsymmetricKeyParameter AKPubKey;
        AsymmetricKeyParameter AKPrivateKey;
        byte[] SmallServerID;

        try {
            AKPubKey =  AsymmetricKeys.loadPublicKey(publicKeyFileName);
        } catch (IOException e) {
            System.err.printf("Could not locate or open PublicKey File. %s \n",publicKeyFileName );
            return null;
        }

        try {
            AKPrivateKey =  AsymmetricKeys.loadPrivateKey(privateKeyFileName);
        } catch (IOException e) {
            System.err.printf("Could not locate or open PrivateKey File. %s \n",privateKeyFileName );
            return null;
        }

        //PublicKeyInfo retten, um Hash-Wert erzeugen zu können
        SmallServerID = createServerIDsmall();
        if(SmallServerID == null){
            System.err.println("Could not create ServerIDsmall.");
            return null;
        }
        return new Keys(AKPubKey,AKPrivateKey,SmallServerID);
    }

     /**
     * Creates ServerIDsmall from SHA256-Hash of Server PublicKey
     * @return  byte[]  The value for ServerIDsmall
     */
    private static byte[] createServerIDsmall() {
        SubjectPublicKeyInfo info;
        byte[] DERinfoHashed ;
        DERBitString DERinfo;
        byte[] result;

        info = AsymmetricKeys.getPubInfo();
        if(info == null){
            System.err.println("Could not extract the PublicKeyInfo.");
            return null;
        }

        DERinfo = info.getPublicKeyData();
        DERinfoHashed = SHA256.hashSHA256(DERinfo.getOctets());

        result= Arrays.copyOfRange(DERinfoHashed, 0,NUM_SERVER_ID_SMALL);
        System.out.printf("ServerIDsmall set to: %s (Len: %d) \n", Util.asHexString(result," "),result.length);
        return result;
    }

    /**
     * Constructor für Keys - > setzen des Schlüsselspaars und des Hashwertes für den Server
     * @param AKPublic           - öffentlicher Schlüssel vom AsymmetricKeyParameter
     * @param AKPrivate          - privater Schlüssel vom Typ AsymmetricKeyParameter
     * @param ServerIDsmall      - ID des Servers in Kurzform
     */
    private Keys(AsymmetricKeyParameter AKPublic, AsymmetricKeyParameter AKPrivate, byte[] ServerIDsmall) {

        this.AKPublic = AKPublic;
        this.AKPrivate = AKPrivate;
        this.ServerIDsmall = ServerIDsmall;
    }

    public AsymmetricKeyParameter GetPublicKey() {
        return AKPublic;
    }

    public AsymmetricKeyParameter GetPrivateKey() {
        return AKPrivate;
    }

    public byte[] getServerIDsmall() {
        return ServerIDsmall;
    }
}

