/*
 * Mqtt Responder. Respond to UIDs broadcasted by an RFID reader. This is educational software.
 * Copyright (C) 2019 Marcus Gelderie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package de.hsaalen.iot.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Util {

    private static byte[] unbox(Byte[] array) {
        var unboxedArray = new byte[array.length];
        for (int i = 0; i < array.length; i++) {
            unboxedArray[i] = array[i];
        }
        return unboxedArray;
    }

    public static Map<UID, byte[]> loadDatabase(String databaseDefaultFilename) {
        try (var reader = Files.newBufferedReader(Path.of(databaseDefaultFilename))) {
            Map<UID, byte[]> database = new HashMap<>();
            String line = reader.readLine();
            while (line != null) {
                var scanner = new Scanner(line);
                if (!scanner.hasNext())
                    continue;
                var uid = UID.fromString(scanner.next());
                if (uid == null)
                    continue;
                List<Byte> list = new LinkedList<>();
                while (scanner.hasNext()) {
                    int nextInt = scanner.nextInt(16);
                    if (nextInt < 0 || nextInt > 255)
                        continue;
                    list.add((byte) nextInt);
                }
                database.put(uid, unbox(list.toArray(new Byte[0])));
                System.out.printf("[+] Successfully loaded data for UID %s.\n", uid);
                line = reader.readLine();
            }
            return database;
        } catch (IOException e) {
            System.err.println("Could not locate or open database.");
            return null;
        }
    }
}
