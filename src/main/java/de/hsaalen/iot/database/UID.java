/*
 * Mqtt Responder. Respond to UIDs broadcasted by an RFID reader. This is educational software.
 * Copyright (C) 2019 Marcus Gelderie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package de.hsaalen.iot.database;

import java.util.Arrays;
import java.util.Scanner;

public class UID {

    public static final int NUM_UID_BYTES = 5;

    private final byte[] bytes;

    public static UID fromBytes(byte[] raw, int offset) {
        if (raw == null) {
            System.err.println("Got null, but need bytes.");
            return null;
        }

        if (offset < 0 || offset >= raw.length) {
            System.err.printf("Error, got an illegal offset: %d\n.", offset);
            return null;
        }

        if (raw.length < offset + NUM_UID_BYTES) {
            System.err.printf("[!] Need %d bytes to form UID, but have %d.\n", NUM_UID_BYTES, raw.length - offset);
            return null;
        }
        return new UID(Arrays.copyOfRange(raw, offset, offset + NUM_UID_BYTES));
    }

    public static UID fromString(String uid) {
        Scanner sc = new Scanner(uid).useDelimiter(":");
        byte[] bytes = new byte[NUM_UID_BYTES];
        for (int i = 0; i < NUM_UID_BYTES; i++) {
            if (!sc.hasNext()) {
                System.err.printf("[!] Need %d bytes to form a UID, but found %d: %s\n", NUM_UID_BYTES, i, uid);
                return null;
            }
            int x = sc.nextInt(16);
            if (x < 0 || x > 255) {
                throw new IllegalArgumentException(String.format("Illegal input. Value %02x is out of range in %s",
                        x, uid));
            }
            bytes[i] = (byte) x;
        }
        return new UID(bytes);
    }

    private UID(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public String toString() {
        String[] byteArray = new String[bytes.length];
        for (int i = 0; i < byteArray.length; i++) {
            byteArray[i] = String.format("%02X", 0xFF & bytes[i]);
        }
        return String.join(":", byteArray);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UID uid = (UID) o;
        return Arrays.equals(bytes, uid.bytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }
}
