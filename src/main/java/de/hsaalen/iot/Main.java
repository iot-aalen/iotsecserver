/*
 * Mqtt Responder. Respond to UIDs broadcasted by an RFID reader. This is educational software.
 * Copyright (C) 2019 Marcus Gelderie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package de.hsaalen.iot;

import de.hsaalen.iot.database.Keys;
import de.hsaalen.iot.database.UID;
import de.hsaalen.iot.database.Util;
import de.hsaalen.iot.mqtt.MqttResponder;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


class Main {

    private static String VERSION = "???";

    private static final String DATABASE_DEFAULT_FILENAME = "./database.txt";
    private static final String PUBLIC_KEY_DEFAULT_FILENAME = "./keys/ServerPubx25519.key.pem";
    private static final String PRIVATE_KEY_DEFAULT_FILENAME = "./keys/ServerPrivx25519.key.pem";


    private static String parseArguments(String[] args) {
        if (args == null || args.length == 0) {
            return "tcp://localhost:1883";
        }
        if ("--version".equals(args[0])) {
            System.out.println("version: " + VERSION);
            System.exit(0);
        }
        Scanner sc = new Scanner(args[0]).useDelimiter(":");
        if (!sc.hasNext()) {
            System.err.printf("Could not parse commandline: %s\n", args[0]);
            System.exit(1);
        }
        final String hostname = sc.next();
        final int port;
        if (sc.hasNext()) {
            port = sc.nextInt();
        } else {
            port = 1883;
        }
        return String.format("tcp://%s:%d", hostname, port);
    }

    public static void main(String[] args) {
        var brokerUri = parseArguments(args);


        System.out.println("[+] Loading database...");
        Map<UID, byte[]> database = Util.loadDatabase(DATABASE_DEFAULT_FILENAME);
        if (database == null) {
            System.err.println("[!] Could not load any database.");
            System.exit(2);
        }
        System.out.println("[+] Database has been loaded.");

        Keys X25519Keys =  Keys.load(PUBLIC_KEY_DEFAULT_FILENAME,PRIVATE_KEY_DEFAULT_FILENAME);
        System.out.println("[+] x509Keys loaded.");

        System.out.printf("[+] Connecting to broker at %s.\n", brokerUri);
        try (MqttClient client = new MqttClient(brokerUri, "client123")) {
            try {
                client.connect();
            } catch (MqttException e) {
                System.err.println("[!] Error connecting to broker.");
                System.exit(3);
            }
            System.out.printf("[+] Successfully connected to broker at %s.\n", brokerUri);
            try {
                var responder = new MqttResponder(new WeakReference<>(client), database, X25519Keys);
                client.setCallback(responder);
                client.subscribe(responder.getTopicName());
            } catch (MqttException e) {
                System.err.printf("[!] Error subscribing to topic.\n" +
                                          "    Reason: %s", e.getMessage());
                System.exit(3);
            }
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(5);
                }
            }
        } catch (MqttException e) {
            System.err.printf("[!] Error creating MQTT client: %s\n", e.getMessage());
            System.exit(3);
        }
    }

}
