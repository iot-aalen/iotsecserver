package de.hsaalen.iot.mqtt;

import de.hsaalen.iot.database.UID;
import de.hsaalen.iot.util.Util;

import java.util.Arrays;

public class Payload {

    private final UID uid;
    private final String messageOriginUUID;

    public static Payload parsePayload(byte[] raw) {
        final var uid = UID.fromBytes(raw, 0);
        if (uid == null) {
            System.err.println("[!] Could not get UID from raw bytes.");
            return null;
        }
        final byte[] messageOriginUUID = Arrays.copyOfRange(raw, UID.NUM_UID_BYTES, raw.length);
        final String messageOriginUID = Util.asHexString(messageOriginUUID, "");
        return new Payload(uid, messageOriginUID);
    }

    private Payload(UID uid, String messageOriginUUID) {
        this.uid = uid;
        this.messageOriginUUID = messageOriginUUID;
    }

    public UID uid() {
        return this.uid;
    }

    public String messageOriginUUID() {
        return this.messageOriginUUID;
    }
}
