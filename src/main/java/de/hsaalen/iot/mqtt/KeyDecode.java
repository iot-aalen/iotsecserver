package de.hsaalen.iot.mqtt;

import de.hsaalen.iot.util.Util;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.PrintStream;
import java.util.Arrays;

/**
 * KeyDecode divides the message into its components and makes them available in the respective getter methods
 * Message-Structure: M =  ServerID (<- First 4 Bytes from SHA256 of Server Public Key)
 *                      || cipher (21 Bytes)
 *                      || authentication tag (16 Bytes)
 *                      || nonce ( 12 Bytes)
 *                      || salt ( 8 Bytes)
 *                      || Public-Key from Reader ( 32 Bytes)
 */

public class KeyDecode {


    public static final int  NUM_CIPHER_BYTES = 21;
    public static final int  NUM_AUTHENTICATION_TAG_BYTES =16;
    public static final int  NUM_NONCE_BYTES =12;
    public static final int  NUM_SALT_BYTES =8;
    public static final int  NUM_PUBLIC_KEY_READER_BYTES =32;

    private static int  num_server_id_small = 4;      // wird aufgrund übergebener Schlüsselänge gesetzt, daher keine
                                                      // Konstante
    private byte[] cipher;
    private byte[] authenticationTag;
    private byte[] nonce;
    private byte[] salt;
    AsymmetricKeyParameter K_Reader_pub;

    /**
     * Divides the message in its components while object is constructing.
     * @param ServerID_small First 4 Byte snipped from SH256-HASH of Server Public Key
     * @param Msg  actual Message
     * @return Object KeyDecode
     * @throws NullPointerException if ServerID_small or msg is null
     */
    public static KeyDecode create (byte[] ServerID_small, byte[] Msg){
        byte[] lServerID;
        byte[] lCipher;
        byte[] lAuthenticationTag;
        byte[] lNonce;
        byte[] lSalt;
        byte[] lPublicKeyReader;
        AsymmetricKeyParameter K_Reader_pub;  // public Key form Reader
        int offset = 0;


        // Check Server ID
        if (ServerID_small == null){
            throw new NullPointerException("Server Id wasn't set.");
        }
        if (ServerID_small.length < 1) {
            System.out.printf("ServerIDsmall must have at least length 1 (%d). Keydecode aborted. \n"
                             ,ServerID_small.length);
            return null;
        }
        num_server_id_small = ServerID_small.length;

        if (Msg == null){
            throw new NullPointerException("payload wasn't set");
        }
        if (Msg.length < (offset+num_server_id_small)) {
            System.out.printf("Message is too short for Server ID (min: %s). Keydecode aborted. \n"
                             ,offset+num_server_id_small);
            return null;
        }
        lServerID = Arrays.copyOfRange(Msg, offset, offset + num_server_id_small);
        if(!Arrays.equals(ServerID_small,lServerID)){
            System.out.printf("Server ID  isn't equals. \n%s != %s \n", Util.asHexString(ServerID_small," ")
                    , Util.asHexString(lServerID," "));
            return null;
        }

        // get Cipher
        offset += num_server_id_small;
        if (Msg.length < (offset+NUM_CIPHER_BYTES)) {
            System.out.printf("Message is too short for Cipher (min: %s). Keydecode aborted. \n"
                             ,offset+NUM_CIPHER_BYTES);
            return null;
        }
        lCipher = Arrays.copyOfRange(Msg, offset, offset + NUM_CIPHER_BYTES);

        // get AUTHENTICATION_TAG
        offset += NUM_CIPHER_BYTES;
        if (Msg.length < (offset+NUM_AUTHENTICATION_TAG_BYTES)) {
            System.out.printf("Message is too short for Authentication Tag  (min: %s). Keydecode aborted. \n"
                             ,offset+NUM_AUTHENTICATION_TAG_BYTES);
            return null;
        }
        lAuthenticationTag = Arrays.copyOfRange(Msg, offset, offset + NUM_AUTHENTICATION_TAG_BYTES);

        // get Nonce
        offset += NUM_AUTHENTICATION_TAG_BYTES;
        if (Msg.length < (offset+NUM_NONCE_BYTES)) {
            System.out.printf("Message is too short for Nonce(min: %s). Keydecode aborted. \n"
                             ,offset+NUM_NONCE_BYTES);
            return null;
        }
        lNonce = Arrays.copyOfRange(Msg, offset, offset + NUM_NONCE_BYTES);

        // get Salt
        offset += NUM_NONCE_BYTES;
        if (Msg.length < (offset+NUM_SALT_BYTES)) {
            System.out.printf("Message is too short for Salt (min: %s). Keydecode aborted. \n"
                             ,offset+NUM_SALT_BYTES);
            return null;
        }
        lSalt = Arrays.copyOfRange(Msg, offset, offset + NUM_SALT_BYTES);

        // Check Public Key from Reader
        offset += NUM_SALT_BYTES;
        if (Msg.length < (offset+NUM_PUBLIC_KEY_READER_BYTES)) {
            System.out.printf("Message is too short for Key (min: %s). Keydecode aborted. \n"
                             ,offset+NUM_PUBLIC_KEY_READER_BYTES);
        }
        lPublicKeyReader = Arrays.copyOfRange(Msg, offset, offset + NUM_PUBLIC_KEY_READER_BYTES);
        K_Reader_pub = createPubKeyReader(lPublicKeyReader);
        if (K_Reader_pub == null ) {
            System.out.printf("Message contains no valid Public Key from Reader (Key: %s). Keydecode aborted. \n"
                             ,Util.asHexString(lPublicKeyReader," "));
            return null;
        }

        // check -> all bytes processed?
        offset += NUM_PUBLIC_KEY_READER_BYTES;
        if(Msg.length != offset){
            PrintStream printf = System.out.printf("The message length is not allowed. (actual Len: %d expected Len %d) \n", Msg.length, offset);
            return null;
        }
        return new KeyDecode(lCipher,lAuthenticationTag,lNonce,lSalt,K_Reader_pub);
    }

    /**
     * Create Public Key, fails when incorrect bytestream passed
     * @param ReaderPubKey PublicKey
     * @return AsymmetricKeyParameter -> null, when fail otherwise -> keyvalue
     */
    private static AsymmetricKeyParameter createPubKeyReader(byte[] ReaderPubKey) {
        AsymmetricKeyParameter K_Reader_pub;
        if (ReaderPubKey == null) {
            return null;
        }
        try {
             K_Reader_pub = PublicKeyFactory.createKey(ReaderPubKey);
        } catch (Exception E) {
            return null;
        }
        return K_Reader_pub;
    }

    /**
     * Constructor from class KeyDecode
     * @param cipher
     * @param authenticationTag
     * @param nonce
     * @param salt
     * @param K_Reader_pub         // Public Key from Reader
     */
    private KeyDecode(byte[] cipher, byte[] authenticationTag, byte[] nonce, byte[] salt, AsymmetricKeyParameter K_Reader_pub){
        this.cipher = cipher;
        this.authenticationTag = authenticationTag;
        this.nonce = nonce;
        this.salt = salt;
        this.K_Reader_pub = K_Reader_pub;
    }

    public byte[] getCipher() {
        return cipher;
    }
    public byte[] getAuthenticationTag() {
        return authenticationTag;
    }
    public byte[] getNonce() {
        return nonce;
    }
    public byte[] getSalt() {
        return salt;
    }
    public AsymmetricKeyParameter getPublicKeyReader() {
        return K_Reader_pub;
    }
}
