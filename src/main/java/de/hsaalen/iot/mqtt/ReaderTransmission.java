package de.hsaalen.iot.mqtt;

public class ReaderTransmission {
    private byte [] readerMessage = new byte[49];

    //concatenate N, C, T in a bytearray
    public byte[] assembleMessage(byte[] nonce, byte[] cipherText, byte[] tag) {
        System.arraycopy(nonce, 0, readerMessage, 0, nonce.length);
        System.arraycopy(cipherText, 0, readerMessage, nonce.length, cipherText.length);
        System.arraycopy(tag, 0, readerMessage, nonce.length + cipherText.length, tag.length);
        return readerMessage;
    }
}
