/*
 * Mqtt Responder. Respond to UIDs broadcasted by an RFID reader. This is educational software.
 * Copyright (C) 2019 Marcus Gelderie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package de.hsaalen.iot.mqtt;


import de.hsaalen.iot.crypto.Encryption;
import de.hsaalen.iot.database.Keys;
import de.hsaalen.iot.database.UID;
import de.hsaalen.iot.crypto.Decryption;
import de.hsaalen.iot.crypto.sharedSecret;
import de.hsaalen.iot.util.Util;
import org.eclipse.paho.client.mqttv3.*;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static de.hsaalen.iot.util.Util.asHexString;

public class MqttResponder implements MqttCallback {

    private static final String IDENTITY_TOPIC = "identity";

    private Map<UID, byte[]> database;
    private Keys x25519Keys;

    private ReaderTransmission readerTransmission = new ReaderTransmission();

    private WeakReference<MqttClient> client;

    public MqttResponder(WeakReference<MqttClient> client, Map<UID, byte[]> database, Keys x25519Keys) {
        if (database == null) {
            throw new NullPointerException("'data' was null");
        }
        this.database = database;

        if (x25519Keys == null) {
            throw new NullPointerException("Server has no keys loaded.");
        }
        this.x25519Keys = x25519Keys;

        if (client == null) {
            throw new NullPointerException("Cannot use null weak reference.");
        }
        this.client = client;

    }

    public String getTopicName() {
        return IDENTITY_TOPIC;
    }

    @Override
    public void connectionLost(Throwable throwable) {
        System.err.println("[!] Lost connection...");
        System.err.printf("    Reason: %s", throwable.getMessage());
        throwable.printStackTrace(System.err);
        var clientRef = this.client.get();
        if (clientRef == null) {
            return;
        }
        try {
            clientRef.connect();
            clientRef.setCallback(this);
            clientRef.subscribe(IDENTITY_TOPIC);
        } catch (MqttException e) {
            System.err.printf("[!] Could not reconnect.\n" + "    Reason: %s", e.getMessage());
            System.exit(128);
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

        if (!this.getTopicName().equals(topic)) {
            return;
        }

        // 1.4 Message parsen und Key-Parm zur Verfügung stellen
        var keyDecode = KeyDecode.create(x25519Keys.getServerIDsmall(), mqttMessage.getPayload());
        if (keyDecode == null) {
            System.out.printf("[Info] Key ignored %s \n", asHexString(mqttMessage.getPayload(), " "));
            return;
        }

        byte[] secret;
        secret = sharedSecret.Agreement(x25519Keys.GetPrivateKey(), keyDecode.getPublicKeyReader());

        // HKDF
        byte[] Key;
        byte[] info = null;
        Key = Decryption.HKDFKey(secret, keyDecode.getSalt(), info);
        
        // Decryption
        byte[] P;
        P = Decryption.AES256GCM_Decrypt(keyDecode.getNonce(), Key, keyDecode.getCipher(), keyDecode.getAuthenticationTag());

        var payload = Payload.parsePayload(P);
        if (payload == null) {
            System.err.println("Could not convert message to a UID.");
            System.err.printf("Message was %s", asHexString(P, " "));
            return;
        }

        // 1.6 und Class encryption einbauen
        byte[] data = database.get(payload.uid());    //encrypt secret before transmission to reader
        if (data == null) {
            System.out.printf("[!] Could not find any data for UID %s.\n", payload.uid());
            return;
        }

        //1.7 encrypt secret with aes-gcm
        //output C,T,N
        //1.8 assemble Message M = N,C,T
        byte[] Nonce = Encryption.BouncyCastleRandom(12);
        byte[] nSecret = Encryption.aesGcmEncrypt(data, Key, 128, Nonce);
        String c;
        String str;
        str = Util.asHexString(nSecret,"");
        c = str.substring(0,str.length()-32);
        byte[] cypher = c.getBytes(StandardCharsets.UTF_8);
        String t;
        t = str.substring(str.length()-32);
        byte[] token = t.getBytes(StandardCharsets.UTF_8);

        //get outputs C,T,N                                       put outputs here -->
        byte [] assMessage = readerTransmission.assembleMessage(Nonce, cypher, token);

        var clientRef = client.get();
        if (clientRef == null) {
            System.out.println("[!] Client ref was already gone...");
            return;
        }

        System.out.printf("[+] Responding to UID %s.\n", payload.uid());
        clientRef.publish(topicFromUUIDHex(payload.messageOriginUUID()), new MqttMessage(assMessage)); //new encrypted payload here instead of secret (assMessage)
    }

    private static String topicFromUUIDHex(String messageOriginUUID) {
        return String.format("%s/response/%s", IDENTITY_TOPIC, messageOriginUUID);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        // not implemented
    }
}
