package de.hsaalen.iot;

public class transform {

    //used in Test Class
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    //used in Test Class
    public static byte[] StringToByte(String P){
            byte[] b = P.getBytes();
            return b;
    }

    public static String byteArrayToHexString(byte[] b1) {
        StringBuilder strBuilder = new StringBuilder();
        for(byte val : b1) {
           strBuilder.append(String.format("%02x", val&0xff));
        }
        return strBuilder.toString();
    }

}