package de.hsaalen.iot;

import java.security.SecureRandom;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.prng.RandomGenerator;
import org.bouncycastle.crypto.prng.VMPCRandomGenerator;

import de.hsaalen.iot.crypto.Decryption;
import de.hsaalen.iot.crypto.Encryption;
import de.hsaalen.iot.crypto.sharedSecret;
import de.hsaalen.iot.crypto.AsymmetricKeys;
import de.hsaalen.iot.transform;

public class Test {

    public static void main(String[] args) throws Exception {
        
        //Secret X
        String privKey = "C:/ServerPrivx25519.key.pem";
        String pubKey = "C:/TestPubx25519.key.pem";
        String privKeyTest = "C:/TestPrivx25519.key.pem";
        String pubKeyTest = "C:/ServerPubx25519.key.pem";

        AsymmetricKeyParameter priv = AsymmetricKeys.loadPrivateKey(privKey);
        AsymmetricKeyParameter pub = AsymmetricKeys.loadPublicKey(pubKey); //only for testing
        AsymmetricKeyParameter privTest = AsymmetricKeys.loadPrivateKey(privKeyTest);
        AsymmetricKeyParameter pubTest = AsymmetricKeys.loadPublicKey(pubKeyTest);
        // here the Pub Key from the MQTT Message
        //System.out.println(priv);
        //System.out.println(pub);

        byte[] x;
        byte[] x2;
        String secret;
        String secret2;
        x = sharedSecret.Agreement(priv, pub);
        secret = transform.byteArrayToHexString(x);
        x2 = sharedSecret.Agreement(privTest, pubTest);
        secret2 = transform.byteArrayToHexString(x2);
        System.out.println(secret);
        System.out.println(secret2);
        if(secret.equals(secret2) == true){
            System.out.println("Testing Secret: true");
        }
        else{
            System.out.println("Test Secret: failed");
        }


        
        //HKDF TEST
        /*String sharedsecret = "79656c6c6f77207375626d6172696e65";
        int length = 8
        byte[] salt;
        salt = Encryption.BouncyCastleRandom(8);
        //String salt = transform.byteArrayToHexString(salt);
        //String salt = "0000000000000000";
        String info = "";
        byte[] shared;
        byte[] sal;
        byte[] inf;
        byte[] Ke;
        
        shared = transform.hexStringToByteArray(sharedsecret);
        //sal = transform.hexStringToByteArray(salt);
        inf = transform.hexStringToByteArray(info);
        Ke = Decryption.HKDFKey(shared, salt, inf);
        String key;
        key = transform.byteArrayToHexString(Ke);
        System.out.println(key);

        // AES256GCM TEST Decryption
        String Key = "79656c6c6f77207375626d6172696e65";
        String Nonce = "000000000000000000000000";
        String Cypher = "908ec9c86aa01929";
        String Tag = "364ef9d491705eb784d2984904e0a31c";
        byte[] N;
        byte[] K;        
        byte[] C;
        byte[] T;
        byte[] P;

        N = transform.hexStringToByteArray(Nonce);
        K = transform.hexStringToByteArray(Key);
        C = transform.hexStringToByteArray(Cypher);
        T = transform.hexStringToByteArray(Tag);

        P = Decryption.AES256GCM_Decrypt(N, K, C, T);
        String s = new String(P);
        System.out.println(s);*/

        
        // AES256GCM TEST Encryption
        /*byte[] C;
        String Plain = "deadbeef";
        String Key = "79656c6c6f77207375626d6172696e65";
        String Nonce = "000000000000000000000000";
        byte[] K;
        byte[] N;
        byte[] P;

        int length2 = 12;
        byte[] newNonce;
        newNonce = Encryption.BouncyCastleRandom(length2);
        String newNonceHex = transform.byteArrayToHexString(newNonce);
        
        System.out.println(newNonceHex);

        K = transform.hexStringToByteArray(Key);
        N = transform.hexStringToByteArray(Nonce);
        P = transform.StringToByte(Plain);
        //aad = transform.hexStringToByteArray(AAD);
        
        
        C = Encryption.aesGcmEncrypt(P, K, 128, newNonce); 
        System.out.println(C);
        String str;
        str = transform.byteArrayToHexString(C);
        System.out.println(str);


        //Split
        String cypher;
        cypher = str.substring(0,str.length()-32);
        System.out.println("Cypher: "+ cypher);

        String token;
        token = str.substring(str.length()-32,str.length());
        System.out.println("Token: "+ token);*/

    }


}   