/*
 * Mqtt Responder. Respond to UIDs broadcasted by an RFID reader. This is educational software.
 * Copyright (C) 2019 Marcus Gelderie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package de.hsaalen.iot.util;

public class Util {
    public static String asHexString(byte[] array, String delimiter) {
        // this is far from the fastest way of doing this, but it is good enough for us
        String[] hex = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            hex[i] = String.format("%02x", array[i]);
        }
        return String.join(delimiter, hex);
    }
}
