package de.hsaalen.iot;

        import de.hsaalen.iot.database.Keys;
        import de.hsaalen.iot.mqtt.KeyDecode;

        import java.io.IOException;

public class TestJens {
    private static final String PUBLIC_KEY_DEFAULT_FILENAME = "./keys/ServerPubx25519.key.pem";
    private static final String PRIVATE_KEY_DEFAULT_FILENAME = "./keys/ServerPrivx25519.key.pem";


    public static void main(String[] args) throws IOException {
        byte[] msg1 = new byte [] {
               //         1           2            3            4 ServerID
                 (byte) 0x2d,(byte) 0x7F, (byte) 0x53, (byte) 0xA6
               // 1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 Chiper
                 ,1 ,2 ,3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21
               // 1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 Authentication Tag
                 ,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45
               // 1  2  3  4  5  6  7  8  9  10 11 12 Nonce
                 ,60,61,62,63,64,65,66,67,68,69,70,71
               // 1  2  3  4  5  6  7  8 Salt
                 ,81,82,83,84,85,86,87,88
               // 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 E(K R/pub)
                ,90,91,92,93,94,95,96,97,98,50,51,52,53,54,55,56,56,57,58,59,23,24,25,26,27,28,29,47,48,49,72,99
        };
        KeyDecode kd = null;
        Keys k =  Keys.load(PUBLIC_KEY_DEFAULT_FILENAME,PRIVATE_KEY_DEFAULT_FILENAME);
        System.out.printf("Geladener PublicKey: %s \n",k.GetPublicKey() );
        System.out.printf("Geladener PrivateKey: %s \n",k.GetPrivateKey() );
        //system.out.printf("Hex PublicKey: %s \n", Util.asHexString())

        try {
            kd = KeyDecode.create(k.getServerIDsmall(), msg1 );
        } catch (Exception e) {
            System.err.println("Fehler beim dekodieren des Keys aufgetreten");
        };

        if (kd == null) {
           System.out.println("KD didn't works");
        } else {
            System.out.println("KD was successfull");
        }
       //byte[] test = new MqttResponder();
    }
}

