package de.hsaalen.iot.crypto;



import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.RuntimeCryptoException;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.generators.HKDFBytesGenerator;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.HKDFParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.Arrays;


public class Decryption {

    public static byte[] HKDFKey(byte[] sharedsecret, byte[] salt, byte[] info) {
        
        HKDFBytesGenerator hkdf = new HKDFBytesGenerator(new SHA256Digest());
        hkdf.init(new HKDFParameters(sharedsecret, salt, info));
        byte[] key = new byte[32];
        hkdf.generateBytes(key, 0, 32);
        return key;
        
    }
 
    public static byte[] AES256GCM_Decrypt(byte[] N, byte[] K, byte[] C, byte[] T){ // N : Nonce , K : Key, C : Cypher, T : Authentication Tag

        try {
                GCMBlockCipher cipher = new GCMBlockCipher(new AESEngine());
                AEADParameters parameters = new AEADParameters(new KeyParameter(K), T.length * 8, N);
                cipher.init(false, parameters);

                byte[] out = new byte[cipher.getOutputSize(C.length + T.length)];

                int pos = cipher.processBytes(C, 0, C.length, out, 0);
                pos += cipher.processBytes(T, 0, T.length, out, pos);
                pos += cipher.doFinal(out, pos);

                return Arrays.copyOf(out, pos);

            } catch (IllegalStateException | InvalidCipherTextException | RuntimeCryptoException ex) {
                throw new IllegalStateException("GCM decrypt error", ex);
            }
        }

}
