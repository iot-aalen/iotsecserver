package de.hsaalen.iot.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256 {

      //hash function returns byte[]
    public static byte[] hashSHA256(byte[] input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(input);
            return md.digest();
        }
        catch (NoSuchAlgorithmException e){
            System.out.println("SHA-256 hashing went wrong. Error: " + e);
            return null;
        }
    }

}


