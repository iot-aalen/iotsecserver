package de.hsaalen.iot.crypto;

import org.bouncycastle.crypto.agreement.X25519Agreement;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class sharedSecret {

    public static byte[] Agreement(AsymmetricKeyParameter priv, AsymmetricKeyParameter pub){

        X25519Agreement agree = new X25519Agreement();
        agree.init(priv);
        byte[] secret = new byte[agree.getAgreementSize()];
        agree.calculateAgreement(pub, secret, 0);
        return secret;
    }
    
}