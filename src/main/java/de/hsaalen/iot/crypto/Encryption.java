package de.hsaalen.iot.crypto;

import java.security.SecureRandom;

import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.prng.RandomGenerator;
import org.bouncycastle.crypto.prng.VMPCRandomGenerator;

public class Encryption {

    public static byte[] aesGcmEncrypt(byte[] P, byte[] K, int atLength, byte[] N) throws Exception {
        BlockCipher blockCipher = new AESEngine();
        blockCipher.init(true, new KeyParameter(new SecretKeySpec(K, "AES").getEncoded()));
    
        GCMBlockCipher aGCMBlockCipher = new GCMBlockCipher(blockCipher);
        aGCMBlockCipher.init(true, new AEADParameters(new KeyParameter(K), atLength, N));
    
        int len = aGCMBlockCipher.getOutputSize(P.length);
        byte[] out = new byte[len];
        int outOff = aGCMBlockCipher.processBytes(P, 0, P.length, out, 0);
        aGCMBlockCipher.doFinal(out, outOff);
    
        return out;
    }

 
    private final static RandomGenerator random = new VMPCRandomGenerator();

    public static byte[] BouncyCastleRandom(int length) {
        long t = System.currentTimeMillis();                    //t?
        byte[] seed = new SecureRandom().generateSeed(length);
        random.addSeedMaterial(seed);
        return seed;
    }
}