/*
 * Mqtt Responder. Respond to UIDs broadcasted by an RFID reader. This is educational software.
 * Copyright (C) 2019 Marcus Gelderie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package de.hsaalen.iot.crypto;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@SuppressWarnings("WeakerAccess")
public class AsymmetricKeys {
    private static SubjectPublicKeyInfo pubInfo = null;
    public static SubjectPublicKeyInfo getPubInfo(){
        return pubInfo;
    }

    @SuppressWarnings("unused")
    public static AsymmetricKeyParameter loadPublicKey(String filename) throws IOException {
        PemReader reader = new PemReader(Files.newBufferedReader(Path.of(filename)));
        var pemObject = reader.readPemObject();
        pubInfo = SubjectPublicKeyInfo.getInstance(pemObject.getContent());
        return PublicKeyFactory.createKey(pubInfo);
    }

    @SuppressWarnings("unused")
    public static AsymmetricKeyParameter loadPrivateKey(String filename) throws IOException {
        PemReader reader = new PemReader(Files.newBufferedReader(Path.of(filename)));
        var pemObject = reader.readPemObject();
        var info = PrivateKeyInfo.getInstance(pemObject.getContent());
        return PrivateKeyFactory.createKey(info);
    }
}
