package de.hsaalen.iot;

import de.hsaalen.iot.mqtt.ReaderTransmission;

public class TestAlex {

    public static void main(String[] args) {
        byte[] nonce = new byte[]{1,2,3,4,5,6,7,8,9,10,11,12};
        byte[] cipherText = new byte[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21};     //length?
        byte[] tag = new byte[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};           //length?
        ReaderTransmission readerTransmission = new ReaderTransmission();


        byte[] readerMessage = readerTransmission.assembleMessage(nonce, cipherText, tag);


        for (int i = 0; i < readerMessage.length; i++) {
            System.out.println("readerMessage: " + readerMessage[i]);
        }
    }
}
